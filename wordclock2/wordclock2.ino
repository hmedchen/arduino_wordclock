// WORDCLOCK
// Version tracking in GIT.
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Date and time functions using a DS3231 RTC connected via I2C and Wire lib
#include <Wire.h>
#include <RTClib.h>

//-------------- DEFINES -------------
// PINS
#define PIN 6 // data pin for LED strip
#define PC_PIN 0 // photo cell pin

#define MAX_BRIGHTNESS 255
#define B_VBRIGHT 1
#define B_BRIGHT 0.8
#define B_LIGHT 0.6
#define B_DIM 0.3
#define B_DARK 0.1
#define BRIGHTNESS_DELTA 25  // Hysterese (10 previously)

#define DEBUG false
#define DEBUG_TIME_INCREASE 300

// update frequency of clock (in 1/1000s)
#define FADE_TIME 1000
#define FADE_STEPS 50
#define RAINBOW_SPEED 500

// layout of the clock
#define COLS 11
#define ROWS 10 //temp

#define NUM_PIXELS 114 //ROWS*COLS+4
#define START_MIN_LEDS 110 // "corner" LEDS are at the end of the strip

// color weights, as the strip isn't balanced in color
#define WEIGHT_R 1
#define WEIGHT_G 0.8
#define WEIGHT_B 0.7

#define MINS_ELEMENTS 3 // amount of array members per minute entry
#define PREFIX_ELEMENTS 2 // array members for 'PREFIX' words

#define BLACK 0 //background: off
#define DARK 1 //foreground: warmwhite
#define DIM 2
#define LIGHT 3
#define BRIGHT 4
#define VERY_BRIGHT 5


//-------------- GLOBALS and INITS -------------
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_PIXELS, PIN, NEO_GRB + NEO_KHZ800);
RTC_DS3231 rtc;

uint8_t wait=200;

// layout of clock
const char german[COLS][ROWS] = {
    "ESKISTLFÜNF",  //0
    "ZEHNZWANZIG",  //1
    "DREIVIERTEL",  //2
    "TGNACHVORJM",  //3
    "HALBQZWÖLFP",  //4
    "ZWEINSIEBEN",  //5
    "KDREIRHFÜNF",  //6
    "ELFNEUNVIER",  //7
    "WACHTZEHNRS",  //8
    "BSECHSFMUHR"}; //9
  // define hours by col, row and length, start with twelve
  
struct wordpos{
    byte row;
    byte col; 
    byte len;};

wordpos prefix[2]={{0,0,2},{0,3,3}};

wordpos hours[12]={
    {4,5,5}, // ZWOELF
    {5,2,4}, // EINS
    {5,0,4}, // ZWEI
    {6,1,4}, // DREI
    {7,7,4}, // VIER
    {6,7,4}, // FUENF
    {9,1,5}, // SECHS
    {5,5,6}, // ZIEBEN
    {8,1,4}, // ACHT
    {7,3,4}, // NEUN
    {8,5,4}, // ZEHN
    {7,0,3}  // ELF
  };

const wordpos mins[12][3] ={
    {{9,8,3},{0,0,0},{0,0,0}}, // ES IST <S>  X  <UHR>   (bei EINS muss das S bei der vollen Stundenangabe weggelassen werden)
    {{0,7,4},{3,2,4},{0,0,0}}, // FÜNF NACH   X
    {{1,0,4},{3,2,4},{0,0,0}}, // ZEHN NACH   X
    {{2,4,7},{3,2,4},{0,0,0}}, // VIERTEL NACH   X
    {{1,4,7},{3,2,4},{0,0,0}}, // ZWANZIG NACH   X
    {{0,7,4},{3,6,3},{4,0,4}}, // FÜNF VOR HALB   X+1
    {{4,0,4},{0,0,0},{0,0,0}}, // HALB   X+1
    {{0,7,4},{3,2,4},{4,0,4}}, // FÜNF NACH HALB   X+1
    {{1,4,7},{3,6,3},{0,0,0}}, // ZWANZIG VOR   X+1
    {{2,0,11},{0,0,0},{0,0,0}}, // DREIVIERTEL   X+1
    {{1,0,4},{3,6,3},{0,0,0}}, // ZEHN VOR   X+1
    {{0,7,4},{3,6,3},{0,0,0}} // FÜNF VOR   X+1  
  };


// ------  COLORS

uint32_t colors[6];

// Array of all pixels with new and old color -  needed for fading
uint8_t rgb_old[NUM_PIXELS];
uint8_t rgb_new[NUM_PIXELS];

uint8_t col_ON=DIM;
uint8_t col_OFF=BLACK;

//String a = "";
char istring[12];
byte isize = 0;

bool rainbow_mode = false;
uint16_t rainbow_pos = 0;
/*
# basics:
colorsets.append(['white',255,255,255])
colorsets.append(['silver',192,192,192])
colorsets.append(['gray',100,100,100])
colorsets.append(['off',0,0,0])
colorsets.append(['black',0,0,0])
colorsets.append(['red',255,0,0])
colorsets.append(['green',0,255,0])
colorsets.append(['blue',0,0,255])
colorsets.append(['yellow',255,255,0])
colorsets.append(['cyan',0,255,255])
colorsets.append(['magenta',255,0,255])
colorsets.append(['darkred',100,0,0])
colorsets.append(['darkgreen',0,100,0])
colorsets.append(['darkblue',0,0,100])
colorsets.append(['purple',100,0,100])
colorsets.append(['olive',100,100,0])
colorsets.append(['teal',0,100,100])

# whites:
colorsets.append(['candle',255,147,41])
colorsets.append(['warmwhite',255,214,170])
colorsets.append(['halogen',255,241,224])
colorsets.append(['warmwhite2',255,197,143])
colorsets.append(['skyblue',135,206,235])

# others:
colorsets.append(['chocolate',139,69,19])
colorsets.append(['forestgreen',34,139,34])
colorsets.append(['orange',255,165,0])
colorsets.append(['gold',255,215,0])
colorsets.append(['cadetblue',95,158,160])
colorsets.append(['minimal',6,6,6])

 */
//========================================
//-------------- FUNCTIONS ---------------
//========================================


//-----------------------------------------
// Set new, wished color for a pixel
//-----------------------------------------
void setPixel(int i, uint8_t col)
{
    rgb_new[i]=col;
}

// Return color components
uint8_t Red(uint32_t color) {
    return uint8_t((color >> 16) & 0xFF);
}
uint8_t Green(uint32_t color) {
    return uint8_t((color >> 8) & 0xFF);
}
uint8_t Blue(uint32_t color) {
    return uint8_t(color & 0xFF);
}

//-----------------------------------------
// Next fading step for all pixels
//-----------------------------------------
void fade(int t)
{
  uint8_t wait_time = uint8_t(t / FADE_STEPS);
  int i;
  int step;
  uint8_t red,green,blue;
  
  for (step=1; step <= FADE_STEPS; step++) // for each fade time
  {
    for (i=0; i<NUM_PIXELS; i++)
    {
      // code from worldclock.ino.
      if (colors[rgb_new[i]] != colors[rgb_old[i]]) {
        red = Red(colors[rgb_old[i]])
            +  uint32_t(float(( Red(colors[rgb_new[i]]) - Red(colors[rgb_old[i]])) / float(FADE_STEPS)) * float(step));
        green = Green(colors[rgb_old[i]])
            + uint32_t(float((Green(colors[rgb_new[i]]) - Green(colors[rgb_old[i]])) / float(FADE_STEPS)) * float(step));
        blue = Blue(colors[rgb_old[i]])
            + uint32_t(float((Blue(colors[rgb_new[i]]) - Blue(colors[rgb_old[i]]))/ float(FADE_STEPS)) * float(step));
        strip.setPixelColor(i, red, green, blue);
      }
    }
    strip.show();
    delay(wait_time);
  }
  for (i=0; i<NUM_PIXELS; i++)
  {
     strip.setPixelColor(i,colors[rgb_new[i]]);
     rgb_old[i]=rgb_new[i];
  }
  strip.show();
  delay(1000);
}

//-----------------------------------------
// Rainbow mode
//-----------------------------------------
void rainbow(uint16_t wait_time)
{
  uint16_t i;
  
  for (i=0; i< NUM_PIXELS; i++) {
    if (colors[rgb_new[i]] != colors[col_OFF]) {
      strip.setPixelColor(i, Wheel(rainbow_pos+i));
    }
  }
  if (rainbow_pos >= 255) {
    rainbow_pos = 0;      
  }
  else {
    rainbow_pos += 1;
  }

  strip.show();
  delay (wait_time);
}

uint32_t setColor(uint8_t r, uint8_t g, uint8_t b)
{
  return strip.Color(WEIGHT_R*r, WEIGHT_G*g, WEIGHT_B*b);
}

DateTime getTime()
{
  DateTime now = rtc.now();
  return DateTime(now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second());
}

//-----------------------------------------
// translate row/col into pixel count
//-----------------------------------------
void SetClockPos (wordpos pos, uint8_t color)
{
   int i=0;
   int n=0;
 
   for (n=0; n<pos.len; n++)
   {
      i=((pos.col + n)*ROWS);  // i is at least number of full columns * number of COLS;
      if ( ((pos.col + n )% 2) == 0) // if col is even
      {
        i+=pos.row;
      }
      else
      {
         i+=ROWS-1-pos.row;   
      }

      setPixel(i,color);
   }
}


//-----------------------------------------
// printTime ()
// prints time on serial out
//-----------------------------------------
void printTime(DateTime now) {
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    if (now.minute() < 10) {
      Serial.print('0');
    }
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    if (now.second() < 10) {
      Serial.print('0');
    }
    Serial.println(now.second(), DEC);
}

//-----------------------------------------
// ShowTimeWords ()
// actual word clock - lights up pixels according to given time
//-----------------------------------------
void ShowTimeWords (DateTime t)
{
  static int prefix_prev=false; // previous prefix ("es ist")
  static int h_prev=-1; // prev hours
  static int m_prev=-1;  // prev min(word)
  static int m2_prev=-1; // prev mins
  
  int h=t.hour();
  int m=t.minute();
  int min_index=0;
  int min_left=0;
  int i;
  wordpos tmp_h;


  // --- set ALL Pixel to off
  for (i=0; i<NUM_PIXELS; i++) {
    setPixel(i, col_OFF);
  }
  
  // --- Show prefix
  for (i=0; i<PREFIX_ELEMENTS; i++)  // Show prefix
  { 
    SetClockPos(prefix[i], col_ON); 
  }

  // --- show minutes
  min_index=m / 5;
  min_left= m % 5;

  for (i=0; i<MINS_ELEMENTS; i++)  // go through all again
  { 
    // if full hour, add suffix ('Uhr')
    if (min_index == 0) {
      SetClockPos(mins[0][i], col_ON);  // set new
    }
    // actual minute term
    SetClockPos(mins[min_index][i], col_ON);  // set new
  }
  m_prev=min_index;
 
  // --- Show hours
  // Hour adjustments 
  if (min_index >=5) { // adjust hours for some of the indexes
    h +=1;
  }
  // reduce hours to h12
  if (h >=24) { // happens only once a day - caused problems on the 12
    h-=24;
  }
  if (h >=12) {
    h-=12;
  }

  tmp_h=hours[h];
  if (h == 1 && min_index == 0)   // special case for german 'one'
  {
   // bei 'EIN UHR' muss die 'EINS' um eins gekuerzt werden  
     tmp_h.len-=1;
  }
  SetClockPos(tmp_h, col_ON);
  h_prev = h;

  // --- Show minute dots
  for (i=0; i<min_left; i++)
  {
     setPixel(START_MIN_LEDS+i, col_ON);
  }
}

//-----------------------------------------
// setBrightnesss ()
// returns brightness level
//-----------------------------------------
int setBrightness()
{
  static int prev_bright=-1;

  // read photocell
  int pc_read = analogRead (PC_PIN);
  Serial.print("Analog reading = ");
  Serial.print(pc_read);  

  if (prev_bright <= pc_read)   // reduce by 10 if prev value was less, to prevent flickering in border cases
  {
    pc_read -= BRIGHTNESS_DELTA;
  }
  else // ditto for more
  {
    pc_read += BRIGHTNESS_DELTA;
  }
  
  if (pc_read < 40) {
    Serial.println(" - Dark");
    col_ON=DARK;
  } else if (pc_read < 200) {
    Serial.println(" - Dim");
    col_ON=DIM;
  } else if (pc_read < 600) {
    Serial.println(" - Light");
    col_ON=LIGHT;
  } else if (pc_read < 800) {
    Serial.println(" - Bright");
    col_ON=BRIGHT;
  } else {
    Serial.println(" - Very bright");
    col_ON=VERY_BRIGHT;
  }
}

void parseCmd(char *timestring, byte timesize) {
  Serial.print("I received: '");
  Serial.print(timestring);
  Serial.println("'");

  char *cmd;
  char *param;
  int nparam;
  cmd = strtok(timestring, " \n");
  if(cmd != NULL) {
    param = strtok(NULL," \n");
    param[2] = 0;
    nparam = atoi(param);
  }
  
  // Setting hour
  if (strcmp(cmd,"hour") == 0) {
    if (nparam >= 0 && nparam < 23){
      Serial.print("Setting hour to ");
      Serial.println(nparam);
      DateTime now = rtc.now();
      rtc.adjust(DateTime(now.year(), now.month(), now.day(), nparam, now.minute(), now.second()));
    }
    else {
      Serial.println("Minutes out of bound (0-59)!");
    }    
  }
  
  // Setting minute
  else if (strcmp(cmd, "min") == 0) {
    if (nparam >= 0 && nparam < 60){
      Serial.print("Setting minute to ");
      Serial.println(nparam);
      DateTime now = rtc.now();
      rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour(), nparam, 0));
    }
    else {
      Serial.println("Minutes out of bound (0-59)!");
    }
  }
  
  else if (strcmp(cmd, "rainbow") == 0) {
    Serial.println("Rainbow Mode on!");
    rainbow_mode = true;
  }

  else if (strcmp(cmd, "normal") == 0) {
    Serial.println("Rainbow Mode off :-(");
    rainbow_mode = false;
  }

  
  // rainbow
  else if (strcmp(cmd, "disco") == 0) {
    Serial.println("DISCO!!!");
    rainbowCycle(2);
  }
}

//===============================================
// SETUP
//===============================================
void setup() {
  // --- init color definitions
  uint32_t col_black=setColor(0,0,0); //background: off
  uint32_t col_candle_dark=setColor(177,74,21);
  uint32_t col_candle=setColor(255,147,41);
  uint32_t col_chocolate=setColor(255,214,170); //foreground: warmwhite
  uint32_t col_warmwhite=setColor(255,197,143);
  uint32_t col_bluewhite=setColor(212,235,255);
  colors[BLACK]=col_black;
  colors[DARK]=col_candle_dark;
  colors[DIM]=col_candle;
  colors[LIGHT]=col_chocolate;
  colors[BRIGHT]=col_warmwhite;
  colors[VERY_BRIGHT]=col_bluewhite;

  // --- init pixel colors ---
  for (int i=0; i<NUM_PIXELS; i++)
  {
    rgb_old[i]=colors[col_OFF];
    rgb_new[i]=colors[col_OFF];
  }

  // --- init serial out
  Serial.begin(57600);

  // --- init real time clock DS3231
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2020, 8, 12, 00, 00, 0));
  }

  
  // --- init Neopixel array
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  // --- print welcome message and serial input commands
  Serial.println("Welcome.");
  Serial.println("The following commands are available:");
  Serial.println("- hour [0-23]  # Sets hour of the clock");
  Serial.println("- min [0-59]   # Sets minute of the clock");
  Serial.println("- disco on       # rainbow test");
  Serial.println("- rainbow on       # rainbow mode on");
  Serial.println("- normal on       # rainbow mode off");
  
  // --- LED test
  rainbowCycle(1);
  strip.clear();
  strip.show(); // Initialize all pixels to 'off'
}

//===============================================
// L O O P
//===============================================
void loop() 
{
  static DateTime now;
  istring[0] = 0;
  isize = 0;
  
  setBrightness();
  
  // read serial input, if available
  if(Serial.available()) 
  {
    // reading command
    isize = Serial.readBytes(istring,10);
    // terminating command (minus cr/lf)
    istring[isize] = 0;
    // clearing rest of the buffer
    while(Serial.available() > 0) {
      char t = Serial.read();
    }
    // parsing command
    parseCmd(istring, isize);
  }
  
  now = getTime();
  printTime(now);  // prints the time on serial
  ShowTimeWords (now);

  if (DEBUG)   // in DEBUG mode, advance clock faster
  {
    now = now + DEBUG_TIME_INCREASE; 
  }

  if (rainbow_mode == false) {
    // Wait one second before repeating :)
    fade(FADE_TIME);
    // delay handled in fade()
  }
  else {
    rainbow(FADE_TIME);
  }
}


//===============================================
// GOODIE functions
//===============================================

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(RAINBOW_SPEED);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
